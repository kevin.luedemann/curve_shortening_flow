Helper Methods
==============

.. automethod:: curve_shortening_flow.evaluation.length

.. automethod:: curve_shortening_flow.evaluation.center_mass

.. automethod:: curve_shortening_flow.evaluation.bounding_box
