Usage iterator
==============

This guide is meant to help setting up the :doc:`iterator <./curve_shortening_flow/iterator_class>`. 
It can already be used with the default setting as described in the :doc:`example <demo_iterator>`, but in order to set it up more easily, some flags are available.
The chapters are separated into *Flags*, *parameters* and *time step methods* as part of the parameters.

Flags
-----

The flags can be set with the class instance, for example

.. code-block:: python

   vert = curve_shortening_flow.iterator.iterator.example("rectangle")
   vert.remesh = False

deactivates the remeshing.

The flags are

* remeshing
   Redistribute the vertices along the curve.
   This keeps the iteration stable by preventing clustering of the points.
   If the number of vertices is to high for a save iteration the number is reduced, which does not effect the curve.
   The default setting is **True** but for the :doc:`higher order schemes <./curve_shortening_flow/paper>` it is deactivated, because the have this feature build in.

* remove
   Removing vertices that get to close to be iterated. 
   This is needed to keep the iteration stable. 
   The default setting is **False**, because the remeshing takes care of the vertex distance.

* save
   Saving the vertices of every step along with some helpful variables like *length*, *CM-position* and *curvatures*.
   They can be addressed later by there names and used for plotting specific information like length over time.
   The Default setting is **True**, but this can lead to a large memory consumption.

* move_cm
   Moving the CM back to it original position.
   Due to the removing and remeshing of vertices the CM moves, which is undesirable.
   This is just a patch, not a fix, because now lines, where the curvature is infinite are moving as well.
   If a correct view of not moving lines is required set this option to False.
   Default setting is **True**, because is looks nicer.

* cubic
   The spline interpolation for remeshing is by default set to cubic, which means this flag is set to **True**.
   Cubic is more accurate to smooth curves, which leads to wrong results if the curve contains sharp corners, like in the *rectangle*.
   The initial curve after initialization is remeshed once with linear splines to assure proper distances between the vertices.

* rescale_length
   The curve shortening flow does not include boundaries as in a minimal surface iterator.
   Therefore the curve will contract to a single point and the flow is getting harder to observe.
   This can be corrected by using a length rescaling once the minimal length is reached.
   The default setting is **True**.


Parameters
----------

Some parameters can be adjusted to assure better performance or different results.
The most important parameter is the time stepping *nethod*.
With this parameter on sets which :doc:`time step sheme <./curve_shortening_flow/time_step>` is used.
The default method is **euler_forward** because it is the simples and therefor fastest but not the most accurate stepping method.
This method choses a time step with which the curve is safely iterated without oscillations.

If a more sophisticated method is required, some more :doc:`stable methods <./curve_shortening_flow/paper>` are also available.
They are labeled in the same way as described

* timestep31
* timestep32
* timestep33
* timestep34

If these schemes are chosen, the remeshing is turned of, because these methods self stabilize by a tangential velocity.

Other parameters include

* tolerance
   If the length is rescaled, the curve will not collapse and therefore a minimum amount of vertices is left in the curve.
   This can lead to an infinite loop.
   To avoid that a minimal change in the mean curvature is added.
   The tolerance is the smallest change in the mean curvature at which the curve does not change any longer.
   The default is set to **1e-10**, because it lead with some examples to the best trade of between number of iterations and circular shape.

* min_length
   If the length is rescaled, some minimal length must be set.
   This is by default done by **1/4 of the bounding box**, but it can be set to any length.
   The :doc:`bounding_box <./curve_shortening_flow/evaluate>` method setting this is described in the evaluations.

.. todo::

   Add a more accurate measure for stopping the infinite loop, because the mean curvature does not behave nicely for most of the examples.
