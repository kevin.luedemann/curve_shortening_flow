How to HMI
==========

The :doc:`HMI <./curve_shortening_flow/hmi>` (Human machine interface) is a class to directly draw some curves.
A :doc:`demo <demo_hmi>` for this class is available in the demo folder in the root directory.

To use the interface, get a class object

.. code:: python

   hmi = curve_shortening_flow.hmi.get_vertices()

and start the drawing process

.. code:: python

   hmi.drawing().

A figure should appear.
The drawing is done with the mouse.
A click with the left mouse button creates a vertex.
If clicked and hold, the drawing continues by moving the mouse.
New vertices will appear in a minimal distance to the last vertex.
By releasing the left mouse button the drawing process is paused, but can be continued by again holding and moving.

In order to draw straight lines a key stroke is necessary.
By moving the cursor to a position and pressing either the *b* or *v* key a line will appear with the minimal vertex distance.
This feature will only work after an initial vertex has been set, because otherwise no vertices can be connected.

The drawing and vertices can be cleared by pressing the *c* key.
The drawing can be finished by pressing either *q* or *esc*.
This closes the figure and lets the code continue with further use of these vertices.
They can be interfaced by

.. code:: python

   hmi.vert

and used to initialize an iterator.

The minimal distance between vertices can be initially set by setting the optional *radius* keyword or later via

.. code:: python

   hmi.radius = 3.

During the drawing the radius can be controlled by the number keys.
The higher the number, the fewer the vertices.
Which means the initially set radius is multiplied by the number, default is *3*.

The last vertex drawn before the session was stopped is getting connected with the first vertex in the same way but with the maximum number of points possible (factor of 1).
This can increase the number of points drastically if the first and last vertex are far apart.

It is possible that some functions return warnings or errors because of mishandling.
Most of theme are handled but some still print into commandline, which usually does not interrupt the functionality.
If during drawing the cursor leaves the drawing area, an error is caught and the next vertex is at the point where the cursor reentered the drawing area.
