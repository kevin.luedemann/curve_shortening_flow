HMI
===

The HMI (Human machine interface) is a class for drawing curves and generating vertices.
A guide on how to use it can be found here :doc:`How to HMI <../hmi>`.

This class initializes a figure and connects to mouse and keyboard inputs.
These are then used to control the drawing and calculation of the new vertices.

A future update might include a way to load images and create curves by manually setting vertices.
An automatic way of generating the vertices is included in :doc:`get_vertices.py <get_vertices>` but still needs some work.

.. autoclass:: curve_shortening_flow.hmi.get_vertices
   :members:
