import numpy as np
#the matplotlib backend if required needs to be set before the import of 
#curv_shortening_flow, because it needs the matplotlib as well
import matplotlib.pyplot as plt
import curve_shortening_flow

def main():
    #getting instance of the HMI
    hmi = curve_shortening_flow.hmi.get_vertices()
    #starting the drawing process
    #from here on the figure handles everything until it is closes
    hmi.drawing()

    #just some feedback of the amount of vertices
    print(hmi.n)

    #getting a iterator instance initialized with the vertices
    #from the HMI
    vert = curve_shortening_flow.iterator.iterator(hmi.vert)

    #iterating the vertices until converged
    vert.steps()
    #loading animation feature with default parameters
    #the figure,axis and animation have to be saved locally
    #in order to be displayed by plt.plot()
    fanim,ax_anim,anim_line = vert.animate()

    #opening second plot with own plotting
    f,ax = plt.subplots(1,2)
    #plotting the normalized length as a function of time
    ax[0].semilogy(vert.times,vert.lengths/vert.lengths[0],'r')
    ax[0].set_ylabel("length/l$_0$")
    ax[0].set_xlabel("time")
    ax[0].set_title("length of curve normalized")
    #plotting the position of the CM
    ax[1].plot(vert.cms[:,0],vert.cms[:,1],'k+-')
    ax[1].set_title("center of mass position")

    #show all plots
    #animation and local plot
    plt.show()

if __name__ == "__main__":
    main()
