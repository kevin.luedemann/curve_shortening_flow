Iterator Class
==============

This is the Class for the iterator which is the main part of the module.
It initializes every important variable and array.
After or while that process the flow can be processed and animated.
As a remark only the so far saved curve flow can be animated but the animation can be restarted a any point.
The figure and axis are always returned and need to be saved in a local variable for the plot to show up.
Otherwise they are destroyed and can not be displayed by the matplotlib.
Also only the backends *qt5agg* and *qt4agg* has been tested so far.

.. todo::

   Write the animation in a way that the user can provide an update method.
   By this it is more easy to personalize to animation.


.. autoclass:: curve_shortening_flow.iterator.iterator
   :members:

