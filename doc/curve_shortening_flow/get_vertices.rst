Get Vertices
============

Methods for obtaining or generating vertices to iterate.

.. todo::

   There is a method for creating vertices from a picture, but it does not work properly. 
   On has to use the right pixel distance for the points an hope the picture has high enough resolution. 
   This might need to be a project on it's own.

.. automethod:: curve_shortening_flow.get_vertices.make_ellipse

.. automethod:: curve_shortening_flow.get_vertices.make_rectangle

.. automethod:: curve_shortening_flow.get_vertices.shift

.. automethod:: curve_shortening_flow.get_vertices.load_example

.. automethod:: curve_shortening_flow.get_vertices.get_next_vertex

.. automethod:: curve_shortening_flow.get_vertices.get_vertices
