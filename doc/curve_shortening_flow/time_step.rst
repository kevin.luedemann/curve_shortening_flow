Stepping time
=============

These are helper methods for the time step.
They are mainly used for the euler step, because the :doc:`higher order schemes <paper>` are self stabilising.

.. automethod:: curve_shortening_flow.time_step.remesh

.. automethod:: curve_shortening_flow.time_step.remove

.. automethod:: curve_shortening_flow.time_step.rescale_length

The simplest possible time step method is

.. automethod:: curve_shortening_flow.time_step.euler_forward
