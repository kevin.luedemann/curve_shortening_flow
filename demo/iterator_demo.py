import matplotlib.pyplot as plt
import numpy as np
import curve_shortening_flow

def main():
    #loading the example and getting a class instance
    vert = curve_shortening_flow.iterator.iterator.example("rectangle")
    #iterating until converged
    vert.steps()
    #creating the animation and saving figure, axis and animation
    #animation running with 50 frames per second
    f_anim,ax_anim,line_ani = vert.animate(FPS=50)

    #creating local figure
    f,ax = plt.subplots(1,2)
    #plotting normalized length as a function of time
    ax[0].semilogy(vert.times,vert.lengths/vert.lengths[0],'r')
    ax[0].set_ylabel("length/l$_0$")
    ax[0].set_xlabel("time")
    ax[0].set_title("length of curve normalized")
    #Plotting the change in mean curvature as a function of time.
    #The algorithm convergence tolerance is default at a change of
    #less then 1e-10
    ax[1].semilogy(vert.times,
        np.abs(vert.mean_curv-np.roll(vert.mean_curv,-1)),'k-')
    ax[1].set_title("mean curvature change")

    #displaying both, the animation and the local plot
    plt.show()

if __name__ == "__main__":
    main()
