Basic idea
==========

In the following we work with a discrete closed curve :math:`\gamma: \mathbb{R}\rightarrow \mathbb{R}^2`.
What this program does is to take this curve and determine the surface of the minimal area that has this curve as boundary.
Because we only do this in the 2-dimensional case, this is always a point. 
So what we expect is that the algorithms transform the curve fist into a circle and shrinks this circle to a point. 


Discrete in this case means that we do not know the whole curve but therefore some points called vertices :math:`\lbrace v_1,...v_n\rbrace\in(\mathbb{R}^2)^n`. 
If we would have infinite vertices they would fit the curve exactly. 
The more points we have the smoother the algorithms works.
We define the length which is a important value

.. math:: len(\gamma)=\sum\limits_{i=1}^n \parallel v_{i+1}-v_i\parallel.

Note that :math:`v_{n+1}=v_{1}`. 
Now we take a variation :math:`\lbrace \eta_1,...,\eta_n\rbrace\in (\mathbb{R}^2)^n`. 
Now we compute

.. math:: 
	\frac{d}{d\epsilon}|_{\epsilon=0} len(\gamma+\epsilon\eta)\\
	=\frac{d}{d\epsilon} \sum\limits_{i=1}^n \parallel v_{i+1}+\epsilon\eta_{i+1}-v_i-\epsilon\eta_i\parallel\\
	=-\sum\limits_{i=1}^n <-t_{i+1}+t_i,\eta_i>\\
	\Rightarrow \Delta \epsilon|_i=-(t_i-t_{i+1}).

The definition of t is quite easy, it is the normalized difference between two points

.. math:: t_i=\frac{v_i-v_{i-1}}{\parallel v_i-v_{i-1}\parallel}.

The main idea is now that we compute :math:`dx_i=\Delta \epsilon|_i=-(t_i-t_{i+1})` and afterwards :math:`v_i^{new}=v_i+step*dx` where step is the stepsize which is topic of a different section, mainly for the :doc:`higher order schemes <paper>`, because the simplest scheme iterates with the maximal save stepsize.
