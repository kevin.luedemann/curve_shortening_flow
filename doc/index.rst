.. curve_shortening_flow documentation master file, created by
   sphinx-quickstart on Wed Jan 24 10:49:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to curve_shortening_flow's documentation!
=================================================

Introduction
------------

The API `curve_shortening_flow <https://gitlab.gwdg.de/kevin.luedemann/curve_shortening_flow>`_ was developed for a project at the institute of numerics at the University of Göttingen and is hosted on the gitlab page of the GWDG.
This work was done with help from `Dr. Jochen Schulz <http://num.math.uni-goettingen.de/~schulz/>`_ and `Prof. Dr. Max Wardetzky <http://num.math.uni-goettingen.de/~wardetzky/>`_.
It contains an iterator for `curve shortening flow <https://en.wikipedia.org/w/index.php?title=Curve-shortening_flow&oldid=821138340>`_.
It is designed to take an array of vertices and iterating them.
The resulting flow can be animated afterwards.
Also some interesting numbers can be plotted as time series depending on the simulation time (cumulation of the dt).
These include *length*, *CM*-position or *mean curvature*.
The vertices can either be iterated until they collapse to a point, until they converge to a circle of specific size or for a user specifiable amount of steps.

This guide explains the usage and API itself with help of some examples.

Installation Instructions
-------------------------

In order to install the package **pip** is required.
The usual installing is done locally

	pip install -e .

or for users without root privileges

	pip install --user -e .

The option *-e* is used for a link into the API of the root directory.
By this procedure an update of the API by pulling the Repo will be automatically included.

.. todo::

   Upload tho API to pypi.

Compiling the documentation
---------------------------

The documentation is build with `sphinx <http://www.sphinx-doc.org/en/stable/index.html>`_ and in order to compile it the API must be installed and the command

	make html

executed in the *doc* directory.
Other types are supported as well like *latex*, *epup*, *man* and many more.
To see which formats are possible, use

	make help

Examples and demos
------------------
Some examples can be found in the demo folder of the root directory.

1. :doc:`HMI <demo_hmi>` 
2. :doc:`iterator class <demo_iterator>`
3. :doc:`Higher order schemes from paper <demo_paper>`

They are minimum examples to get the API working and try it out.
A in-depth explanation of the API is provided in this document as well and in the docstrings.

Some hand drawn demonstration vertices are provided with their PNG files in the curve_shortening_flow/examples folder.
Also a method for loading these into the iterator class is provided and used for an initialization of the class.
An example result video my look like this

.. raw:: html

   <video controls src="_static/bohne.mp4" width="640" height="480"></video>

This is a bean (in German "bohne") example.
It is a curve with two different directions of curvature.
On can see that near the end of the video the curve contracts to a point.

It is possible to fix the minimal length, which is shown in the next example

.. raw:: html

   <video controls src="_static/rectangle.mp4" width="640" height="480"></video>

This is a rectangle contracting until a minimal length is reached and then it continues to form a circle.

.. todo::

   Prepare more interesting examples and add them to the API, like a log spiral.


Content and API overview
------------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./curve_shortening_flow/Basic idea.rst
   ./curve_shortening_flow/paper.rst
   hmi.rst
   iterator_how_to.rst
   demo_hmi.rst
   demo_iterator.rst
   demo_paper.rst
   ./curve_shortening_flow/iterator_class.rst
   ./curve_shortening_flow/derivative.rst
   ./curve_shortening_flow/time_step.rst
   ./curve_shortening_flow/evaluate.rst
   ./curve_shortening_flow/animation.rst
   ./curve_shortening_flow/get_vertices.rst
   ./curve_shortening_flow/hmi.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. todolist::
