Derivatives
===========

The methods are needed to calculated the values from our :doc:`basic idea <Basic idea>` and they do exactly that.

.. automethod:: curve_shortening_flow.derivative.get_t

.. automethod:: curve_shortening_flow.derivative.deriva
