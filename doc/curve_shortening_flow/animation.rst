Animation Update
================

The Function is the update method for the animation.
It has proven to be enough but for better animation on has to write its own update function and pass it on to the animation class in the :doc:`iterator class <iterator_class>`.

.. automethod:: curve_shortening_flow.animation.animate.update_line
