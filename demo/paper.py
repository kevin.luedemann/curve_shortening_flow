import numpy as np
import matplotlib.pyplot as plt
import curve_shortening_flow

def main():
    #loading the example and getting a class instance
    vert = curve_shortening_flow.iterator.iterator.example("bohne")
    #chosing the 32 time stepping method
    vert.method = "timestep32"
    #iterating until converged
    vert.steps(stps=6000)
    #creating the animation and saving figure, axis and animation
    #animation running with 50 frames per second
    f_anim,ax_anim,line_ani = vert.animate(FPS=50)

    #creating local figure
    f,ax = plt.subplots(1,2)
    #plotting normalized length as a function of time
    ax[0].semilogy(vert.times,vert.lengths/vert.lengths[0],'r')
    ax[0].set_ylabel("length/l$_0$")
    ax[0].set_xlabel("time")
    ax[0].set_title("length of curve normalized")
    #plotting the position of the CM
    ax[1].plot(vert.cms[:,0],vert.cms[:,1],'k+-')
    ax[1].set_title("center of mass position")
    
    #displaying both, the animation and the local plot
    plt.show()
    

if __name__ == "__main__":
    main()

